Тестовое задание.

АПИ для просмотра списка тарифов и оформления заказа на определённый тариф.

Тестовые данные в фикстурах.

Авторизация ч.з. WSSE, список пользователей и паролей в security.yml
security:
     providers:
         wsse_users:
             memory:
                 users:
                     - { name: 'user', password: 'password' }
                     - { name: 'partner', password: 'password' }


Генератор X-WSSE заголовка: http://www.teria.com/~koseki/tools/wssegen/

Пример CURL запроса на получение списка тарифов
curl -v -XGET --header 'X-WSSE: UsernameToken Username="user", PasswordDigest="jTCDJfwIQr97XtKzWeI+wkfY0ss=", Nonce="Y2IyOTc1YTcyMjEwZjQ2Yw==", Created="2016-10-26T13:01:59Z"' "http://127.0.0.1:8000/api/list"

Пример CURL запроса на оформление заказа
curl -v -XPOST --header 'X-WSSE: UsernameToken Username="user", PasswordDigest="jTCDJfwIQr97XtKzWeI+wkfY0ss=", Nonce="Y2IyOTc1YTcyMjEwZjQ2Yw==", Created="2016-10-26T13:01:59Z"' --data 'hosting_plan=free' "http://127.0.0.1:8000/api/list"

Сгенерированная документация
http://127.0.0.1:8000/api_doc
