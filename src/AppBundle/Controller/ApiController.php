<?php

namespace AppBundle\Controller;

use AppBundle\Entity\HostingOrder;
use AppBundle\Entity\HostingPlan;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\DateTime;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * @Route("/api")
 */
class ApiController extends Controller
{

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Список тарифов",
     *  output="json",
     *  statusCodes={
     *      200="Returned when successful",
     *      401="Returned on invalid authentication",
     *      500="Returned on internal server error",
     *  },
     *  section = "Partner API"
     * )
     *
     * @return Response
     * @Route("/list", name="api_list", methods={"GET"})
     */
    public function listAction(Request $request)
    {
        $serializer = $this->get('jms_serializer');
        $em = $this->getDoctrine()->getManager();
        $data = $em->getRepository('AppBundle:HostingPlan')->findAll();

        return new Response($serializer->serialize($data, 'json'));
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Оформление заказа",
     *  output="json",
     *  statusCodes={
     *      200="Returned when successful",
     *      401="Returned on invalid authentication",
     *      500="Returned on internal server error",
     *  },
     *  filters={
     *      {"name"="hosting_plan", "dataType"="string"},
     *  },
     *  section = "Partner API"
     * )
     *
     * @return Response
     * @Route("/order", name="api_create_order", methods={"POST"})
     */
    public function orderAction(Request $request)
    {
        $this->get('logger')->info(sprintf("request: %s", print_r($request->request->all(), true)));

        $em = $this->getDoctrine()->getManager();

        try {
            $order = $this->createOrder($request);

            $errors = $this->get('validator')->validate($order);

            if (count($errors) > 0) {
                throw new \Exception($this->get('error.extractor.validation')->extract($errors), 100);
            }

            $em->persist($order);
            $em->flush();

        } catch (\Exception $e) {
            $this->get('logger')->error(sprintf('Code: "%s". Message: "%s". Trace: %s', $e->getCode(), $e->getMessage(), $e->getTraceAsString()));

            return new JsonResponse([
                'status' => 'fail',
                'error_code' => $e->getCode(),
                'error_description' => $e->getMessage(),
            ]);
        }

        return new JsonResponse(['status' => 'success']);
    }

    private function createOrder(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var HostingPlan $hosting_plan */
        $hosting_plan = $em->getRepository('AppBundle:HostingPlan')->findOneBy(['slug' => $request->request->get('hosting_plan')]);

        if (!$hosting_plan) {
            throw new \Exception('Wrong hosting plan', 100);
        }

        $this->get('logger')->info(sprintf("plan: %s, user: %s", $hosting_plan->getName(), $this->getUser()));

        return
            (new HostingOrder())
                ->setUser($this->getUser())
                ->setHostingPlan($hosting_plan)
                ->setCreated(new \DateTime())
        ;
    }

}