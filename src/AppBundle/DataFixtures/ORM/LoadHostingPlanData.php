<?php
namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\HostingPlan;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadHostingPlanData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $hp = new HostingPlan();
        $hp->setName('Free');
        $hp->setSlug('free');
        $hp->setDescription('Free trial 14 days.');
        $hp->setPrice(0);
        $manager->persist($hp);

        $hp = new HostingPlan();
        $hp->setName('Basic');
        $hp->setSlug('basic');
        $hp->setDescription('256Mb, 10Gb, unlimited traffic.');
        $hp->setPrice(10);
        $manager->persist($hp);

        $hp = new HostingPlan();
        $hp->setName('Premium');
        $hp->setSlug('premium');
        $hp->setDescription('1024Mb, 100Gb, unlimited traffic.');
        $hp->setPrice(50);
        $manager->persist($hp);

        $manager->flush();
    }
}