<?php
namespace AppBundle\EventListener;

use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class AppExceptionListener
{

    private $container;
    private $logger;

    public function __construct($container, $logger)
    {
        $this->container = $container;
        $this->logger = $logger;
    }

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        // You get the exception object from the received event
        /** @var \Exception $e */
        $e = $event->getException();

        $this->logger->error("Exception detected: " . $e->getMessage(), [
            'line' => $e->getLine(),
            'err_code' => $e->getCode(),
            'trace' => $e->getTraceAsString(),
        ]);

        return new JsonResponse([
            'status' => 'fail',
            'error_code' => $e->getCode(),
            'error_description' => $e->getMessage(),
        ]);
    }
}