<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * HostingOrder
 *
 * @ORM\Table(name="hosting_order")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\HostingOrderRepository")
 */
class HostingOrder
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\HostingPlan")
     * @ORM\JoinColumn(name="hosting_plan_id", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank()
     */
    private $hostingPlan;

    /**
     * @var int
     *
     * @ORM\Column(name="user", type="string", nullable=false)
     * @Assert\NotBlank()
     */
    private $user;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     * @Assert\NotBlank()
     */
    private $created;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set hostingPlan
     *
     * @param integer $hostingPlan
     *
     * @return HostingOrder
     */
    public function setHostingPlan($hostingPlan)
    {
        $this->hostingPlan = $hostingPlan;

        return $this;
    }

    /**
     * Get hostingPlan
     *
     * @return int
     */
    public function getHostingPlan()
    {
        return $this->hostingPlan;
    }

    /**
     * Set user
     *
     * @param integer $user
     *
     * @return HostingOrder
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return int
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return HostingOrder
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }
}

